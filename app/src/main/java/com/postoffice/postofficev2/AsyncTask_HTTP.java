package com.postoffice.postofficev2;

import android.os.AsyncTask;

import org.json.JSONException;

/**
 * Created by peiranhsu on 2015/7/19.
 */
public class AsyncTask_HTTP extends AsyncTask<String, Void, String> {
    private String TAG = "AsyncTask_HTTP";

    @Override
    protected String doInBackground(String... params) {
        String result = "";

        HTTPProcessor httpProcessor = new HTTPProcessor(params[0]);

        httpProcessor.setsServiceName(params[0]);

        try {
            result = httpProcessor.setupRequest();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}

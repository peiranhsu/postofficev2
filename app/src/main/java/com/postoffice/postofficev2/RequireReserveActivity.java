package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


public class RequireReserveActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "RequireReserveActivity";

    private EditText mEditText_phoneNum;

    Button mButton_newReserve;
    Button mButton_requireReserve;
    Button mButton_cancwlReserve;
    Button mButtonConfirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_require_reserve);

        mEditText_phoneNum = (EditText) findViewById(R.id.editText_phoneNum);

        mButton_newReserve = (Button) findViewById(R.id.button_newReserve);
        mButton_requireReserve= (Button) findViewById(R.id.button_requireReserve);
        mButton_cancwlReserve = (Button) findViewById(R.id.button_cancelReserve);
        mButtonConfirm = (Button) findViewById(R.id.button_confirm);


        mButton_newReserve.setOnClickListener(this);
        mButton_requireReserve.setOnClickListener(this);
        mButton_cancwlReserve.setOnClickListener(this);
        mButtonConfirm.setOnClickListener(this);

        mButton_newReserve.setOnTouchListener(this);
        mButton_requireReserve.setOnTouchListener(this);
        mButton_cancwlReserve.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_get_number);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();

        switch (view.getId()){
            case R.id.button_newReserve:
                intent.putExtra("beforeActivity", "Main");
                intent.setClass(this, HttpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                break;
            case R.id.button_requireReserve:
                intent.setClass(RequireReserveActivity.this, RequireReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_cancelReserve:
                intent.setClass(RequireReserveActivity.this, CancelReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_confirm:
                StringProcessor.getInstance().sPhoneNo = mEditText_phoneNum.getText().toString();

                Log.v(TAG, "sPhoneNo = " + StringProcessor.getInstance().sPhoneNo);

                if(StringProcessor.getInstance().isMobileNumber(StringProcessor.getInstance().sPhoneNo) == true){
                    intent.setClass(RequireReserveActivity.this, HttpActivity.class);
                    intent.putExtra("beforeActivity", "RequireReserve");
                    startActivity(intent);
                }
                else {
                    Toast toast = Toast.makeText(RequireReserveActivity.this, "電話號碼錯誤", Toast.LENGTH_SHORT);
                    toast.show();
                }

                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_newReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_new_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_new_reserve);
                }
                break;
            case R.id.button_requireReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_require_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_require_reserve);
                }
                break;
            case R.id.button_cancelReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }
                break;
            case R.id.button_confirm:
                break;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent = new Intent();

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(this, MainActivity.class);

            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }
}

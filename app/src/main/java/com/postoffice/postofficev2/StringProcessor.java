package com.postoffice.postofficev2;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by peiranhsu on 2015/8/27.
 */
public class StringProcessor {
    private static StringProcessor instance;

    String sPhoneNo = null;
    String sSMSGUID = null;
    String sSMSContent = "SMSContent";
    String sSendDate = "2015/08/31";
    String sSendTime = "19:00";
    String sSQ = null;

    //DepInfo
    int iDepClass1Selected = 0;
    int iDepClass2Selected = 0;
    int iDepClass3Selected = 0;
    int iDepInfoNum = 0;
    int iNumDepClass1 = 0;
    int iNumDepClass2 = 0;
    int iNumDepClass3 = 0;
    String sDepNameClass1[];
    String sDepNameClass2[];
    String sDepNameClass3[];
    String sDepCodeClass1[];
    String sDepCodeClass2[];
    String sDepCodeClass3[];
    String sDepClass1[];
    String sDepClass2[];
    String sDepClass3[];
    String sDepCode[];
    String sDepClass[];
    String sDepName[];
    String sDepAllName[];

    //KindInfo
    int iKindSelected = 0;
    String sKind[];
    String sKindName[];

    //ReserveInfo
    int iDateNum = 0;
    int iPeriodNum[];
    int iDataSelected = 0;
    int iTimeSlotSelected = 0;
    int iPeriod = 0;
    String sDate[];
    String sReservePeriod[];
    String sPeriod_SN[][];
    String sPeriod[][];
    String sRemainPerson[][];

    //Result
    int iCheckReserveResult = 0;
    int iCheckSMSResult = 0;
    boolean bCancelReserveResult = false;
    boolean bSendSMSResult = false;
    boolean bAddReserveResult = false;

    int iCheckReserveResultNum = 0;
    String sCheckReserveResultSQ[];
    String sCheckReserveResultDepName[];
    String sCheckReserveResultKindName[];
    String sCheckReserveResultReserveTime[];
    String sCheckReserveResultPeriod[];

    //Time slot string
    String sTimeSlotList [];

    //Add reserve string
    String sReserveInfoAdd;
    
    //Result GetReserve
    public int iDepInfoNumReserved;
    public String[] sDepCodeReserved;
    public String[] sDepClassReserved;
    public String[] sDepNameReserved;
    public String[] sDepAllNameReserved;
    public int iNumDepClass1Reserved;
    public int iNumDepClass2Reserved;
    public int iNumDepClass3Reserved;
    public String[] sDepClass1Reserved;
    public String[] sDepClass2Reserved;
    public String[] sDepClass3Reserved;

    //Random SMS number
    public String sRandomSMSNumber;

    public static StringProcessor getInstance(){
        if(instance == null){
            instance = new StringProcessor();
        }

        return instance;
    }

    public String randomSMSNumber(){
        char[] chars = "0123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < 5; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        sRandomSMSNumber = sb.toString();

        return sRandomSMSNumber;
    }

    public String buildSMSContent(){;
        StringBuilder sb = new StringBuilder();

        //Produce random SMS number
        randomSMSNumber();

        sb.append("北門郵您好, 預約日期 : " + sDate[iDataSelected] + ", (" + sPeriod[iDataSelected][iTimeSlotSelected] + ")的驗證碼為 : " + sRandomSMSNumber + ". 謝謝.");
        this.sSMSContent = sb.toString();

        SimpleDateFormat sdDate = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdTime = new SimpleDateFormat("HH:mm:ss");

        //取得現在時間
        Date date=new Date();
        Date time=new Date();

        //透過SimpleDateFormat的format方法將Date轉為字串
        sSendDate = sdDate.format(date);
        sSendTime = sdTime.format(time);


        return sSMSContent;
    }

    public boolean isMobileNumber(String mobiles) {
        /*Pattern p = Pattern
                .compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
        Matcher m = p.matcher(mobiles);*/

        boolean isMobileNum = false;

        if(mobiles.matches("[0-9]{10}")){
            if(mobiles.charAt(0) == '0' && mobiles.charAt(1) == '9'){
                isMobileNum = true;
            }
        }

        //return m.matches();

        return isMobileNum;
    }

    /*public void buileString(int num1, int num2){
        sPeriod_SN = new String[num1][num2];
        sPeriod = new String[num1][num2];
        sRemainPerson = new String[num1][num2];
    }*/
}

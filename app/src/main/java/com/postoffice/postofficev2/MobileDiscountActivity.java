package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;


public class MobileDiscountActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "MobileDiscountActivity";

    Button mButton_hotActivity;
    Button mButton_speciStore;

    private int mServiceClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_discount);

        this.mServiceClass = Url_PostOffice.getInstance().MOBILE_DISCOUNT;

        mButton_hotActivity = (Button) findViewById(R.id.button_hot_activity);
        mButton_hotActivity.setOnClickListener(this);
        mButton_hotActivity.setOnTouchListener(this);

        mButton_speciStore = (Button) findViewById(R.id.button_speci_store);
        mButton_speciStore.setOnClickListener(this);
        mButton_speciStore.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_mobile_discount);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()){
            case R.id.button_hot_activity:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][0][0]);
                break;
            case R.id.button_speci_store:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][1][0]);
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_hot_activity:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_hot_activity_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_hot_activity);
                }
                break;
            case R.id.button_speci_store:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_speci_store_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_speci_store);
                }
                break;
        }

        return false;
    }
}

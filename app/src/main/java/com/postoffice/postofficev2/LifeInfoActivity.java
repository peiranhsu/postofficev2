package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;


public class LifeInfoActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "LifeInfoActivity";

    Button mButton_uniTicket;
    Button mButton_bus;
    Button mButton_train;
    Button mButton_hsr;
    private int mServiceClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_info);

        this.mServiceClass = Url_PostOffice.getInstance().LIFE_INFORMATION;

        mButton_uniTicket = (Button) findViewById(R.id.button_uni_ticket);
        mButton_uniTicket.setOnClickListener(this);
        mButton_uniTicket.setOnTouchListener(this);

        mButton_bus = (Button) findViewById(R.id.button_bus);
        mButton_bus.setOnClickListener(this);
        mButton_bus.setOnTouchListener(this);

        mButton_train = (Button) findViewById(R.id.button_train);
        mButton_train.setOnClickListener(this);
        mButton_train.setOnTouchListener(this);

        mButton_hsr = (Button) findViewById(R.id.button_hsr);
        mButton_hsr.setOnClickListener(this);
        mButton_hsr.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_life_lnfo);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()){
            case R.id.button_uni_ticket:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][0][0]);
                break;
            case R.id.button_bus:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][1][0]);
                break;
            case R.id.button_train:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][2][0]);
                break;
            case R.id.button_hsr:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][3][0]);
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_uni_ticket:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_uni_ticket_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_uni_ticket);
                }
                break;
            case R.id.button_bus:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_bus_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_bus);
                }
                break;
            case R.id.button_train:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_train_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_train);
                }
                break;
            case R.id.button_hsr:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_hsr_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_hsr);
                }
                break;
        }

        return false;
    }
}

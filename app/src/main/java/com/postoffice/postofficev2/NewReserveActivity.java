package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;


public class NewReserveActivity extends ActionBarActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, View.OnTouchListener{

    private String TAG = "NewReserveActivity";
    private boolean networkSubReserve_add = false;

    private EditText mEditTextPhoneNumber;
    private Spinner mSpinnerReserveDate;
    private Spinner mSpinnerReserveTime;
    private Spinner mSpinnerPostOffice;
    private Spinner mSpinnerProgressPostOfficePrefix;
    private Spinner mSpinnerProgressPostOffice;
    private Spinner mSpinnerBusinessType;

    Button mButton_newReserve;
    Button mButton_requireReserve;
    Button mButton_cancwlReserve;
    Button mButtonConfirm;

    String sPostOfficeList [];
    String sProgressPostOfficePrefixList [];
    String sProgressPostOfficeList [];



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reserve);

        mEditTextPhoneNumber = (EditText)findViewById(R.id.editText_phoneNum);
        mSpinnerReserveDate = (Spinner) findViewById(R.id.spinner_reserveDate);
        mSpinnerReserveTime = (Spinner) findViewById(R.id.spinner_reserveTime);
        mSpinnerPostOffice = (Spinner) findViewById(R.id.spinner_postOffice);
        mSpinnerProgressPostOfficePrefix = (Spinner) findViewById(R.id.spinner_progressPostOfficePrefix);
        mSpinnerProgressPostOffice = (Spinner) findViewById(R.id.spinner_progressPostOffice);
        mSpinnerBusinessType = (Spinner) findViewById(R.id.spinner_businessType);

        mButton_newReserve = (Button) findViewById(R.id.button_newReserve);
        mButton_requireReserve= (Button) findViewById(R.id.button_requireReserve);
        mButton_cancwlReserve = (Button) findViewById(R.id.button_cancelReserve);
        mButtonConfirm = (Button) findViewById(R.id.button_confirm);

        mSpinnerReserveDate.setOnItemSelectedListener(this);
        mSpinnerReserveTime.setOnItemSelectedListener(this);
        mSpinnerPostOffice.setOnItemSelectedListener(this);
        mSpinnerProgressPostOfficePrefix.setOnItemSelectedListener(this);
        mSpinnerProgressPostOffice.setOnItemSelectedListener(this);
        mSpinnerBusinessType.setOnItemSelectedListener(this);

        mButton_newReserve.setOnClickListener(this);
        mButton_requireReserve.setOnClickListener(this);
        mButton_cancwlReserve.setOnClickListener(this);
        mButtonConfirm.setOnClickListener(this);

        mButton_newReserve.setOnTouchListener(this);
        mButton_requireReserve.setOnTouchListener(this);
        mButton_cancwlReserve.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.v(TAG, "onResume!!!");

        setupSpinner();
        updateTimeSpinner();
    }

    public void setupSpinner(){
        if(StringProcessor.getInstance().sDepClass1 != null) {

            if(MyArrayAdapter.getInstance().aPostOfficeList == null) {
                MyArrayAdapter.getInstance().aPostOfficeList = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item,
                        StringProcessor.getInstance().sDepClass1);

            }else {
                MyArrayAdapter.getInstance().aPostOfficeList.notifyDataSetChanged();
            }

            mSpinnerPostOffice.setAdapter(MyArrayAdapter.getInstance().aPostOfficeList);
        }

        if(StringProcessor.getInstance().sDepClass2 != null) {
            if(MyArrayAdapter.getInstance().aProgressPostOfficePrefixList == null) {
                MyArrayAdapter.getInstance().aProgressPostOfficePrefixList = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item,
                        StringProcessor.getInstance().sDepClass2);
            }else {
                MyArrayAdapter.getInstance().aProgressPostOfficePrefixList.notifyDataSetChanged();
            }

            mSpinnerProgressPostOfficePrefix.setAdapter(MyArrayAdapter.getInstance().aProgressPostOfficePrefixList);
        }

        if(StringProcessor.getInstance().sDepClass3 != null) {
            if(MyArrayAdapter.getInstance().aProgressPostOfficeList == null) {
                MyArrayAdapter.getInstance().aProgressPostOfficeList = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item,
                        StringProcessor.getInstance().sDepClass3);
            }else {
                MyArrayAdapter.getInstance().aProgressPostOfficeList.notifyDataSetChanged();
            }

            mSpinnerProgressPostOffice.setAdapter(MyArrayAdapter.getInstance().aProgressPostOfficeList);
        }

        if(StringProcessor.getInstance().sKindName != null) {
            //Business type
            if(MyArrayAdapter.getInstance().aReserveTypeList == null) {
                MyArrayAdapter.getInstance().aReserveTypeList = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item,
                        StringProcessor.getInstance().sKindName);
            }else {
                MyArrayAdapter.getInstance().aReserveTypeList.notifyDataSetChanged();
            }

            mSpinnerBusinessType.setAdapter(MyArrayAdapter.getInstance().aReserveTypeList);
        }

        if(StringProcessor.getInstance().sDate != null) {
            //Date
            if(MyArrayAdapter.getInstance().aDateList == null) {
                MyArrayAdapter.getInstance().aDateList = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item,
                        StringProcessor.getInstance().sDate);
            }else {
                MyArrayAdapter.getInstance().aDateList.notifyDataSetChanged();
            }

            mSpinnerReserveDate.setAdapter(MyArrayAdapter.getInstance().aDateList);
        }
    }

    public void updateTimeSpinner(){
        if(StringProcessor.getInstance().sTimeSlotList != null) {
            if(MyArrayAdapter.getInstance().aTimeList == null) {
                MyArrayAdapter.getInstance().aTimeList = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_dropdown_item,
                        StringProcessor.getInstance().sTimeSlotList);
            }else {
                MyArrayAdapter.getInstance().aTimeList.notifyDataSetChanged();
            }

            mSpinnerReserveTime.setAdapter(MyArrayAdapter.getInstance().aTimeList);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_get_number);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();

        switch (view.getId()){
            case R.id.button_newReserve:
                intent.setClass(NewReserveActivity.this, NewReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_requireReserve:
                intent.setClass(NewReserveActivity.this, RequireReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_cancelReserve:
                intent.setClass(NewReserveActivity.this, CancelReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_confirm:

                StringProcessor.getInstance().sPhoneNo = mEditTextPhoneNumber.getText().toString();

                if(StringProcessor.getInstance().isMobileNumber(mEditTextPhoneNumber.getText().toString())){
                    StringProcessor.getInstance().sReserveInfoAdd =
                                    StringProcessor.getInstance().sDepCodeClass2[StringProcessor.getInstance().iDepClass2Selected]
                                    + "|" + StringProcessor.getInstance().sDepNameClass2[StringProcessor.getInstance().iDepClass2Selected]
                                    + "|" + StringProcessor.getInstance().sDepCodeClass3[StringProcessor.getInstance().iDepClass3Selected]
                                    + "|" + StringProcessor.getInstance().sDepNameClass3[StringProcessor.getInstance().iDepClass3Selected]
                                    + "|" + StringProcessor.getInstance().sKind[StringProcessor.getInstance().iKindSelected]
                                    + "|" + StringProcessor.getInstance().sDate[StringProcessor.getInstance().iDataSelected]
                                    + "|" + StringProcessor.getInstance().sPeriod[StringProcessor.getInstance().iDataSelected][StringProcessor.getInstance().iTimeSlotSelected]
                                    + "|" + StringProcessor.getInstance().sPeriod_SN[StringProcessor.getInstance().iDataSelected][StringProcessor.getInstance().iTimeSlotSelected]
                                    + "|" + StringProcessor.getInstance().sPhoneNo
                                    + "|" + StringProcessor.getInstance().sKindName[StringProcessor.getInstance().iKindSelected];

                    Log.v(TAG, "sReserveInfoAdd = " + StringProcessor.getInstance().sReserveInfoAdd);

                    intent.setClass(NewReserveActivity.this, HttpActivity.class);
                    intent.putExtra("beforeActivity", "NewReserve");
                    startActivity(intent);
                }
                else{
                    Toast toast = Toast.makeText(NewReserveActivity.this, "電話號碼輸入錯誤", Toast.LENGTH_SHORT);
                    toast.show();
                }

                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()){
            case R.id.spinner_postOffice:
                StringProcessor.getInstance().iDepClass1Selected = (int) id;
                Log.v(TAG, "onItemSelected, spinner_postOffice" + StringProcessor.getInstance().iDepClass1Selected);
                break;
            case R.id.spinner_progressPostOfficePrefix:
                StringProcessor.getInstance().iDepClass2Selected = (int) id;
                Log.v(TAG, "onItemSelected, spinner_progressPostOfficePrefix" + StringProcessor.getInstance().iDepClass2Selected);
                break;
            case R.id.spinner_progressPostOffice:
                StringProcessor.getInstance().iDepClass3Selected = (int) id;
                Log.v(TAG, "onItemSelected, spinner_progressPostOffice" + StringProcessor.getInstance().iDepClass2Selected);
                break;
            case R.id.spinner_businessType:
                StringProcessor.getInstance().iKindSelected = (int) id;
                Log.v(TAG, "onItemSelected, spinner_reserveType" + StringProcessor.getInstance().iKindSelected);
                break;
            case R.id.spinner_reserveDate:
                StringProcessor.getInstance().iDataSelected = (int) id;
                Log.v(TAG, "onItemSelected, spinner_date" + String.valueOf(id));
                updateTimeSpinner();
                break;
            case R.id.spinner_reserveTime:
                StringProcessor.getInstance().iTimeSlotSelected = (int) id;
                Log.v(TAG, "onItemSelected, spinner_reserveTime" + String.valueOf(id));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_newReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_new_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_new_reserve);
                }
                break;
            case R.id.button_requireReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_require_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_require_reserve);
                }
                break;
            case R.id.button_cancelReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }
                break;
        }
        return false;
    }
}

package com.postoffice.postofficev2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;


public class WelcomeActivity extends Activity {
    private String TAG = "WelcomeActivity";

    Timer timer = new Timer(true);
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mHandler = new Handler(){
            int i = 0;
            @Override
            public void handleMessage(Message msg) {
                switch(msg.what){
                    case 1:
                        Intent i =new Intent();
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setClass(WelcomeActivity.this, MainActivity.class);
                        startActivity(i);
                        WelcomeActivity.this.finish();
                        break;
                }
                super.handleMessage(msg);
            }
        };

        timer.schedule(new timerTask(), 2000, 3000);
    }

    public class timerTask extends TimerTask
    {
        public timerTask()
        {
        }

        public void run()
        {
            Message msg = new Message();
            msg.what = 1;
            mHandler.sendMessage(msg);

            this.cancel();
        }
    };
}

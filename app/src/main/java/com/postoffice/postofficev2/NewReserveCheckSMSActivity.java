package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;


public class NewReserveCheckSMSActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "NewReserveGetSMSActivity";

    private EditText mEditTextSMS;

    Button mButton_newReserve;
    Button mButton_requireReserve;
    Button mButton_cancwlReserve;
    Button mButtonConfirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reserve_get_sms);

        mEditTextSMS = (EditText) findViewById(R.id.editText_SMS);

        mButton_newReserve = (Button) findViewById(R.id.button_newReserve);
        mButton_requireReserve= (Button) findViewById(R.id.button_requireReserve);
        mButton_cancwlReserve = (Button) findViewById(R.id.button_cancelReserve);
        mButtonConfirm = (Button) findViewById(R.id.button_confirm);

        mButtonConfirm.setOnClickListener(this);
        mButton_newReserve.setOnClickListener(this);
        mButton_requireReserve.setOnClickListener(this);
        mButton_cancwlReserve.setOnClickListener(this);

        mButton_newReserve.setOnTouchListener(this);
        mButton_requireReserve.setOnTouchListener(this);
        mButton_cancwlReserve.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_get_number);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean compareSMSNumber(){

        Log.v(TAG, "mEditTextSMS = " + StringProcessor.getInstance().sRandomSMSNumber);
        Log.v(TAG, "mEditTextSMSUI = " + mEditTextSMS.getText().toString());


        if(mEditTextSMS.getText().toString().equals(StringProcessor.getInstance().sRandomSMSNumber)){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();

        switch (view.getId()){
            case R.id.button_newReserve:
                intent.setClass(NewReserveCheckSMSActivity.this, NewReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_requireReserve:
                intent.setClass(NewReserveCheckSMSActivity.this, RequireReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_cancelReserve:
                intent.setClass(NewReserveCheckSMSActivity.this, CancelReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_confirm:

                if(compareSMSNumber()){
                    intent.setClass(NewReserveCheckSMSActivity.this, HttpActivity.class);
                    intent.putExtra("beforeActivity", "CheckSMS");
                    startActivity(intent);
                }
                else {
                    Toast toast = Toast.makeText(NewReserveCheckSMSActivity.this, "驗證碼錯誤", Toast.LENGTH_LONG);
                    toast.show();
                }

                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_newReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_new_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_new_reserve);
                }
                break;
            case R.id.button_requireReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_require_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_require_reserve);
                }
                break;
            case R.id.button_cancelReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }
                break;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent = new Intent();

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(this, MainActivity.class);

            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }
}

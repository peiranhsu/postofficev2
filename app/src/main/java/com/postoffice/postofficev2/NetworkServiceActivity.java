package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;


public class NetworkServiceActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "NetworkServiceActivity";
    private int mServiceClass;

    boolean networkSubReserve_add = false;

    LayoutInflater layoutInflater;
    LinearLayout linearLayout_networkService;
    LinearLayout linearLayout_networkServiceBottom;
    LinearLayout linearLayout_networkSubReserve;

    Button mButton_networkReserve;
    Button mButton_networkApply;
    Button mButton_windowPhone;
    Button mButton_lostFind;

    Button mButton_save;
    Button mButton_post;
    Button mButton_insurance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_service);

        this.mServiceClass = Url_PostOffice.getInstance().NETWORK_SERVICE;

        linearLayout_networkService = (LinearLayout) findViewById(R.id.linearLayout_network_service);

        linearLayout_networkServiceBottom = (LinearLayout) getLayoutInflater().inflate(R.layout.network_service_bottom_layout, null);
        linearLayout_networkSubReserve = (LinearLayout) getLayoutInflater().inflate(R.layout.network_sub_reserve_layout, null);

        linearLayout_networkService.addView(linearLayout_networkServiceBottom);

        mButton_networkReserve = (Button) findViewById(R.id.button_network_reserve);
        mButton_networkApply = (Button) findViewById(R.id.button_network_apply);
        mButton_windowPhone = (Button) findViewById(R.id.button_window_phone);
        mButton_lostFind = (Button) findViewById(R.id.button_find_lost);

        mButton_networkReserve.setOnClickListener(this);
        mButton_networkApply.setOnClickListener(this);
        mButton_windowPhone.setOnClickListener(this);
        mButton_lostFind.setOnClickListener(this);

        mButton_lostFind.setOnTouchListener(this);
        mButton_networkApply.setOnTouchListener(this);
        mButton_windowPhone.setOnTouchListener(this);
        mButton_lostFind.setOnTouchListener(this);
    }

    public void setupUI(){
        if(!networkSubReserve_add){
            networkSubReserve_add = true;
            linearLayout_networkService.removeView(linearLayout_networkServiceBottom);

            linearLayout_networkService.addView(linearLayout_networkSubReserve);
            linearLayout_networkService.addView(linearLayout_networkServiceBottom);

            mButton_save = (Button) findViewById(R.id.button_save);
            mButton_post = (Button) findViewById(R.id.button_post);
            mButton_insurance = (Button) findViewById(R.id.button_insurance);

            mButton_save.setOnClickListener(this);
            mButton_post.setOnClickListener(this);
            mButton_insurance.setOnClickListener(this);

            mButton_save.setOnTouchListener(this);
            mButton_post.setOnTouchListener(this);
            mButton_insurance.setOnTouchListener(this);

        }else {
            networkSubReserve_add = false;
            linearLayout_networkService.removeView(linearLayout_networkSubReserve);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);

        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_network_service);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()){
            case R.id.button_network_reserve:
                setupUI();
                break;
            case R.id.button_network_apply:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][1][0]);
                break;
            case R.id.button_window_phone:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][2][0]);
                break;
            case R.id.button_find_lost:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][3][0]);
                break;
            case R.id.button_save:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][0][0]);
                break;
            case R.id.button_post:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][0][1]);
                break;
            case R.id.button_insurance:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][0][2]);
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_network_reserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_network_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_network_reserve);
                }
                break;
            case R.id.button_network_apply:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_network_apply_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_network_apply_push);
                }
                break;
            case R.id.button_window_phone:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_window_phone_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_window_phone);
                }
                break;
            case R.id.button_find_lost:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_find_lost_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_find_lost);
                }
                break;
            case R.id.button_save:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_find_lost_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_network_reserve_save);
                }
                break;
            case R.id.button_post:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_find_lost_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_network_reserve_post);
                }
                break;
            case R.id.button_insurance:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_find_lost_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_network_reserve_insurance);
                }
                break;
        }

        return false;
    }
}

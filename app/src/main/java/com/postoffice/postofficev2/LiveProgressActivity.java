package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class LiveProgressActivity extends ActionBarActivity implements View.OnClickListener{

    private String TAG = "LiveProgressActivity";

    Timer timer = new Timer(true);
    Handler mHandler;

    LinearLayout linearLayout;

    ProgressBar mProgressBar_underProgress;
    TextView mTextView_underProgress;
    Button mButton_backToMain;

    Drawable drawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_progress);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout_liveProgressActivity);

        //linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.linearLayout_liveProgressActivity, null);

        mProgressBar_underProgress = (ProgressBar) findViewById(R.id.progressBar_underProgress);
        mTextView_underProgress = (TextView) findViewById(R.id.textView_underProgress);
        mButton_backToMain = (Button) findViewById(R.id.button_backToMain);

        mButton_backToMain.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        timer.schedule(new timerTask(), 3000, 1000);

        mHandler = new Handler() {
            int i = 0;

            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        timer.cancel();
                        mProgressBar_underProgress.setVisibility(View.INVISIBLE);
                        mTextView_underProgress.setVisibility(View.INVISIBLE);
                        mButton_backToMain.setVisibility(View.INVISIBLE);

                        linearLayout.setBackground(drawable);
                        break;
                }
            }
        };
    }

    private void getImage() throws IOException {

            URL url = new URL(Url_PostOffice.getInstance().URL_liveProgress);
            URLConnection conn = url.openConnection();

            HttpURLConnection httpConn = (HttpURLConnection)conn;
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpConn.getInputStream();

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                drawable = new BitmapDrawable(getResources(), bitmap);
                inputStream.close();
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_life_lnfo);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Log.v(TAG, "onClick");

        Intent intent = new Intent();
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        switch (view.getId()){
            case R.id.button_backToMain:
                timer.cancel();
                intent.setClass(this, MainActivity.class);
                break;
            default:
                break;
        }

        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent = new Intent();

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            timer.cancel();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(this, MainActivity.class);

            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }

    public class timerTask extends TimerTask
    {
        Message msg;

        public timerTask()
        {
            msg = new Message();
        }

        public void run()
        {
            msg.what = 1;

            try {
                getImage();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mHandler.sendMessage(msg);

            this.cancel();
        }
    };
}

package com.postoffice.postofficev2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener, View.OnTouchListener{

    String TAG = "MainActivity";
    Button mButton_getNumber;
    Button mButton_liveProgress;
    Button mButton_networkService;
    Button mButton_businessInfo;
    Button mButton_mobileDiscount;
    Button mButton_lifeInfo;
    Button mButton_postLife;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton_getNumber = (Button) findViewById(R.id.button_getNumber);
        mButton_liveProgress = (Button) findViewById(R.id.button_liveProgress);
        mButton_networkService = (Button) findViewById(R.id.button_networkService);
        mButton_businessInfo = (Button) findViewById(R.id.button_businessInfo);
        mButton_mobileDiscount = (Button) findViewById(R.id.button_mobileDiscount);
        mButton_lifeInfo = (Button) findViewById(R.id.button_lifeInfo);
        mButton_postLife = (Button) findViewById(R.id.button_postLife);

        mButton_getNumber.setOnClickListener(this);
        mButton_liveProgress.setOnClickListener(this);
        mButton_networkService.setOnClickListener(this);
        mButton_businessInfo.setOnClickListener(this);
        mButton_mobileDiscount.setOnClickListener(this);
        mButton_lifeInfo.setOnClickListener(this);
        mButton_postLife.setOnClickListener(this);

        mButton_getNumber.setOnTouchListener(this);
        mButton_liveProgress.setOnTouchListener(this);
        mButton_networkService.setOnTouchListener(this);
        mButton_businessInfo.setOnTouchListener(this);
        mButton_mobileDiscount.setOnTouchListener(this);
        mButton_lifeInfo.setOnTouchListener(this);
        mButton_postLife.setOnTouchListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.v(TAG, "onClick");

        Intent intent = new Intent();

        switch (view.getId()){
            case R.id.button_getNumber:
                intent.putExtra("beforeActivity", "Main");
                intent.setClass(this, HttpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                //intent.setClass(this, NewReserveActivity.class);
                break;
            case R.id.button_liveProgress:
                intent.putExtra("beforeActivity", "Main");
                //intent.setFlags(Intent.);
                //intent.setClass(this, HttpActivity.class);
                intent.setClass(this, LiveProgressActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                //intent.setClass(this, NewReserveActivity.class);
                break;
            case R.id.button_networkService:
                intent.setClass(this, NetworkServiceActivity.class);
                break;
            case R.id.button_businessInfo:
                intent.setClass(this, BusinessInfoActivity.class);
                break;
            case R.id.button_mobileDiscount:
                intent.setClass(this, MobileDiscountActivity.class);
                break;
            case R.id.button_lifeInfo:
                intent.setClass(this, LifeInfoActivity.class);
                break;
            case R.id.button_postLife:
                intent.setClass(this, PostLifeActivity.class);
                break;
            default:
                break;
        }

        if(intent != null) {
            this.startActivity(intent);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_getNumber:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_get_number_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_get_number);
                }
                break;
            case R.id.button_liveProgress:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_live_progress_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_live_progress);
                }
                break;
            case R.id.button_networkService:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_network_service_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_network_service);
                }
                break;
            case R.id.button_businessInfo:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_business_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_business_info);
                }
                break;
            case R.id.button_mobileDiscount:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_mobile_discount_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_mobile_discount);
                }
                break;

            case R.id.button_lifeInfo:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_life_info_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_life_info);
                }
                break;
            case R.id.button_postLife:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_post_life_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_post_life);
                }
                break;
        }
        return false;
    }
}

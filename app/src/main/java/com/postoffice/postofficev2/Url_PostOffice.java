package com.postoffice.postofficev2;

import android.net.Uri;
import android.util.Log;

/**
 * Created by peiranhsu on 2015/7/22.
 */
public class Url_PostOffice {

    private static Url_PostOffice instance = null;
    Uri mUri[][][];
    String mService[][];
    String mSubService[][][];

    public final int NETWORK_SERVICE = 0;
    public final int BUSSINESS_INFORMATION = 1;
    public final int LIFE_INFORMATION = 2;
    public final int MOBILE_DISCOUNT = 3;
    public final int POST_MUSEUM= 4;
    public final int NUM_OF_SERVICE_CLASS = 4;

    public int mNumberOfService[] = {4, 6, 4, 2, 3};
    public int mNumberOfSubService[][] = { {3, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 3, 0, 0}, {0, 0}, {0, 0, 0}};

    public String URL_liveProgress = "http://220.130.210.230/JPG/wait.jpg";
    public String URL_WEBSERVICE = "http://220.130.210.230/ReserveServices/wsReserve.asmx";

    public static Url_PostOffice getInstance()
    {
        if(instance == null)
        {
            instance = new Url_PostOffice();
        }

        return instance;
    }

    private Url_PostOffice()
    {
        Log.i("NUM_OF_SERVICE_CLASS=", Integer.toString(NUM_OF_SERVICE_CLASS));

        //[Tab][Service][Sub Service]
        mUri = new Uri[5][10][10];
        mService = new String[5][10];
        mSubService = new String[5][10][10];

        //1.Service string
        mService[NETWORK_SERVICE][0] = "網路預約";
        mSubService[NETWORK_SERVICE][0][0] = "儲匯業務";
        mSubService[NETWORK_SERVICE][0][1] = "郵務業務";
        mSubService[NETWORK_SERVICE][0][2] = "壽險業務";

        mService[NETWORK_SERVICE][1] = "網路申請";
        mService[NETWORK_SERVICE][2] = "窗口顧客電話預約";
        mService[NETWORK_SERVICE][3] = "郵局失物招領";
        //End of Service string

        //URIs of NETWORK_SERVICE Tab
        mUri[NETWORK_SERVICE][0][0] = Uri.parse("http://tppost.com.tw/s_service.php");
        mUri[NETWORK_SERVICE][0][1] = Uri.parse("http://tppost.com.tw/p_service.php");
        mUri[NETWORK_SERVICE][0][2] = Uri.parse("http://tppost.com.tw/i_service.php");
        mUri[NETWORK_SERVICE][1][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=120122");
        mUri[NETWORK_SERVICE][2][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=1434954388875");
        mUri[NETWORK_SERVICE][3][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=1434954388875");
        //end URIs of NETWORK_SERVICE Tab

        //2.Service string
        mService[BUSSINESS_INFORMATION][0] = "臺北郵局最新消息";
        mService[BUSSINESS_INFORMATION][1] = "郵局利率、匯率";
        mService[BUSSINESS_INFORMATION][2] = "業務查詢專區";
        mService[BUSSINESS_INFORMATION][3] = "查詢懶人包";
        mService[BUSSINESS_INFORMATION][4] = "常用業務QR code查詢";
        mService[BUSSINESS_INFORMATION][5] = "臺北郵局業務Q&A";
        //End of Service string

        //URIs of BUSSINESS_INFORMATION Tab
        mUri[BUSSINESS_INFORMATION][0][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=12010501");
        mUri[BUSSINESS_INFORMATION][1][0] = Uri.parse("http://www.post.gov.tw/post/internet/B_saving/index.jsp?ID=30112");
        mUri[BUSSINESS_INFORMATION][2][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=1397117265635");
        mUri[BUSSINESS_INFORMATION][3][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=1433406773205");
        mUri[BUSSINESS_INFORMATION][4][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=1435298403475");
        mUri[BUSSINESS_INFORMATION][5][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=1435298403475");
        //end of URIs of BUSSINESS_INFORMATION Tab

        //3.Service string
        mService[LIFE_INFORMATION][0] = "統一發票兌獎資訊";

        mService[LIFE_INFORMATION][1] = "臺北市交通/公車/停車資訊";
        mSubService[LIFE_INFORMATION][1][0] = "臺北市政府交通局";
        mSubService[LIFE_INFORMATION][1][1] = "我愛巴士(臺北市公車資訊)";
        mSubService[LIFE_INFORMATION][1][2] = "臺北市停車資訊導引系統";

        mService[LIFE_INFORMATION][2] = "台鐵時刻表";
        mService[LIFE_INFORMATION][3] = "高鐵時刻表";
        //End of Service string

        //URIs of LIFE_INFORMATION Tab
        mUri[LIFE_INFORMATION][0][0] = Uri.parse("http://www.etax.nat.gov.tw/etwmain/front/ETW183W1");
        mUri[LIFE_INFORMATION][1][0] = Uri.parse("http://www.dot.gov.taipei/");
        mUri[LIFE_INFORMATION][1][1] = Uri.parse("http://5284.taipei.gov.tw/");
        mUri[LIFE_INFORMATION][1][2] = Uri.parse("http://tpis.pma.gov.tw/ParkInfo/motorinfo");
        mUri[LIFE_INFORMATION][2][0] = Uri.parse("http://twtraffic.tra.gov.tw/twrail/");
        mUri[LIFE_INFORMATION][3][0] = Uri.parse("http://www.thsrc.com.tw/index.html");
        //end of URIs of LIFE_INFORMATION Tab

        //4.Service string
        mService[MOBILE_DISCOUNT][0] = "台鐵時刻表";
        mService[MOBILE_DISCOUNT][1] = "高鐵時刻表";
        //End of Service string

        //URIs of MOBILE_COUNT Tab
        mUri[MOBILE_DISCOUNT][0][0] = Uri.parse("http://www.post.gov.tw/post/internet/Visa/index2.jsp?ID=108001");
        mUri[MOBILE_DISCOUNT][1][0] = Uri.parse("http://www.post.gov.tw/post/internet/Visa/index2.jsp?ID=108002");

        //5.Service string
        mService[POST_MUSEUM][0] = "郵政博物館臺北門分館";
        mService[POST_MUSEUM][1] = "臺北北門郵局展演場地活動表";
        mService[POST_MUSEUM][2] = "臺北北門郵局展演場地出租訊息公告";
        //End of Service string

        //URIs of POSTOFFICE_NORTH Tab
        mUri[POST_MUSEUM][0][0] = Uri.parse("http://museum.post.gov.tw/post/Postal_Museum/postal_museum/index.jsp?ID=1424747523110");
        mUri[POST_MUSEUM][1][0] = Uri.parse("http://www.post.gov.tw/post/internet/Q_localpost/index.jsp?ID=1434504106959");
        mUri[POST_MUSEUM][2][0] = Uri.parse("http://www.post.gov.tw/post/internet/Real_estate/index.jsp?ID=904&control_type=page&news_no=24301");
        //end of URIs of POSTOFFICE_NORTH Tab

    }
}

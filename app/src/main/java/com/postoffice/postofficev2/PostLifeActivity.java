package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;


public class PostLifeActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "PostLifeActivity";

    Button mButton_northPostOffice;
    Button mButton_northPostOfficeActivity;
    Button mButton_northPostOfficeRent;

    private int mServiceClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_life);

        this.mServiceClass = Url_PostOffice.getInstance().POST_MUSEUM;

        mButton_northPostOffice = (Button) findViewById(R.id.button_north_post_office);
        mButton_northPostOffice.setOnClickListener(this);
        mButton_northPostOffice.setOnTouchListener(this);

        mButton_northPostOfficeActivity = (Button) findViewById(R.id.button_north_post_office_activity);
        mButton_northPostOfficeActivity.setOnClickListener(this);
        mButton_northPostOfficeActivity.setOnTouchListener(this);

        mButton_northPostOfficeRent = (Button) findViewById(R.id.button_north_post_office_rent);
        mButton_northPostOfficeRent.setOnClickListener(this);
        mButton_northPostOfficeRent.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_post_life);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()){
            case R.id.button_north_post_office:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][0][0]);
                break;
            case R.id.button_north_post_office_activity:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][1][0]);
                break;
            case R.id.button_north_post_office_rent:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][2][0]);
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_north_post_office:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_north_post_office_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_north_post_office);
                }
                break;
            case R.id.button_north_post_office_activity:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_north_post_office_activity_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_north_post_office_activity);
                }
                break;
            case R.id.button_north_post_office_rent:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_north_post_office_rent_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_north_post_office_rent);
                }
                break;
        }

        return false;
    }
}

package com.postoffice.postofficev2;

import android.widget.ArrayAdapter;

/**
 * Created by peiranhsu on 2015/9/11.
 */
public class MyArrayAdapter {

    private static MyArrayAdapter instance;

    public static MyArrayAdapter getInstance(){
        if(instance == null){
            instance = new MyArrayAdapter();
        }

        return instance;
    }

    public ArrayAdapter aPostOfficeList;
    public ArrayAdapter aProgressPostOfficePrefixList;
    public ArrayAdapter aProgressPostOfficeList;
    public ArrayAdapter aReserveTypeList;
    public ArrayAdapter aDateList;
    public ArrayAdapter aTimeList;
}

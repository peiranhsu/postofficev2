package com.postoffice.postofficev2;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by peiranhsu on 2015/8/27.
 */
public class JSONProcessor {
    private String TAG = "JSONProcessor";
    private static JSONProcessor instance;

    public static JSONProcessor getInstance(){
        if(instance == null){
            instance = new JSONProcessor();
        }
        return instance;
    }


    public JSONObject newJSONObject(String serviceName) throws JSONException {
        JSONArray JArray= new JSONArray();
        JSONObject jsonObject = new JSONObject();

        switch (serviceName){
            case "AddReserve":
                break;
            case "CancelReserve":
                break;
            case "CheckReserve":
                jsonObject.put("PhoneNo", StringProcessor.getInstance().sPhoneNo);
                break;
            case "CheckSMSStatus":
                jsonObject.put("PhoneNo", StringProcessor.getInstance().sPhoneNo);
                break;
            case "GetReserve":
                jsonObject.put("PhoneNo", StringProcessor.getInstance().sPhoneNo);
                break;
            case "SendSMS":
                jsonObject.put("PhoneNo", StringProcessor.getInstance().sPhoneNo);
                jsonObject.put("SMSContent", StringProcessor.getInstance().sSMSContent);
                jsonObject.put("SendDate", StringProcessor.getInstance().sSendDate);
                jsonObject.put("SendTime", StringProcessor.getInstance().sSendTime);
                break;
            case "Test":
                //jsonObject.put("key", "value");
                break;

            default:
                Log.v(TAG, "default");
                break;
        }

        return jsonObject;
    }

    public synchronized void parseJson(String serviceName, String jsonString) throws JSONException {
        JSONObject obj = null;
        JSONArray dataArray = null;
        JSONArray reservePeriodArray = null;

        if (jsonString == null){
            return;
        }

        obj = new JSONObject(jsonString);

        switch (serviceName){
            case "AddReserve":
                StringProcessor.getInstance().bAddReserveResult = obj.getBoolean("AddReserve");
                Log.v(TAG, String.valueOf(StringProcessor.getInstance().bAddReserveResult));
                break;
            case "CancelReserve":
                StringProcessor.getInstance().bCancelReserveResult = obj.getBoolean("CancelReserve");
                break;
            case "CheckReserve":
                StringProcessor.getInstance().iCheckReserveResult = Integer.parseInt(obj.getString("CheckReserve"));
                break;
            case "CheckSMSStatus":
                dataArray = new JSONArray(jsonString);
                StringProcessor.getInstance().iCheckSMSResult = Integer.parseInt(obj.getString("SendStatus"));
                break;
            case "GetDepInfo":
                String sDepInfo = obj.getString("DepInfo");
                dataArray = new JSONArray(sDepInfo);

                StringProcessor.getInstance().iDepInfoNum = dataArray.length();
                StringProcessor.getInstance().sDepCode = new String[dataArray.length()];
                StringProcessor.getInstance().sDepClass = new String[dataArray.length()];
                StringProcessor.getInstance().sDepName = new String[dataArray.length()];
                StringProcessor.getInstance().sDepAllName = new String[dataArray.length()];

                for (int i = 0; i < dataArray.length(); i++) {
                    //接下來這兩行在做同一件事情，就是把剛剛JSON陣列裡的物件抓取出來
                    //並取得裡面的字串資料
                    //dataArray.getJSONObject(i)這段是在講抓取data陣列裡的第i個JSON物件
                    //抓取到JSON物件之後再利用.getString(“欄位名稱”)來取得該項value
                    //這裡的欄位名稱就是剛剛前面所提到的name:value的name
                    StringProcessor.getInstance().sDepCode[i] = dataArray.getJSONObject(i).getString("DepCode");
                    StringProcessor.getInstance().sDepClass[i] = dataArray.getJSONObject(i).getString("DepClass");
                    StringProcessor.getInstance().sDepName[i] = dataArray.getJSONObject(i).getString("DepName");
                    StringProcessor.getInstance().sDepAllName[i] = dataArray.getJSONObject(i).getString("DepAllName");

                    if(StringProcessor.getInstance().sDepClass[i] != null) {
                        if (StringProcessor.getInstance().sDepClass[i].equals("1")) {
                            StringProcessor.getInstance().iNumDepClass1++;
                        } else if (StringProcessor.getInstance().sDepClass[i].equals("2")) {
                            StringProcessor.getInstance().iNumDepClass2++;
                        } else if (StringProcessor.getInstance().sDepClass[i].equals("3")) {
                            StringProcessor.getInstance().iNumDepClass3++;
                        }
                    }
                    //Log.v(TAG, StringProcessor.getInstance().sDepCode[i] + ", " + StringProcessor.getInstance().sDepClass[i] + ", " + StringProcessor.getInstance().sDepName[i] + ", " + StringProcessor.getInstance().sDepAllName[i]);
                }

                StringProcessor.getInstance().sDepClass1 = new String[StringProcessor.getInstance().iNumDepClass1];
                StringProcessor.getInstance().sDepClass2 = new String[StringProcessor.getInstance().iNumDepClass2];
                StringProcessor.getInstance().sDepClass3 = new String[StringProcessor.getInstance().iNumDepClass3];

                StringProcessor.getInstance().sDepCodeClass1 = new String[StringProcessor.getInstance().iNumDepClass1];
                StringProcessor.getInstance().sDepCodeClass2 = new String[StringProcessor.getInstance().iNumDepClass2];
                StringProcessor.getInstance().sDepCodeClass3 = new String[StringProcessor.getInstance().iNumDepClass3];

                StringProcessor.getInstance().sDepNameClass1 = new String[StringProcessor.getInstance().iNumDepClass1];
                StringProcessor.getInstance().sDepNameClass2 = new String[StringProcessor.getInstance().iNumDepClass2];
                StringProcessor.getInstance().sDepNameClass3 = new String[StringProcessor.getInstance().iNumDepClass3];

                Log.v(TAG, "sDepClass1 = " + StringProcessor.getInstance().iNumDepClass1);
                Log.v(TAG, "sDepClass2 = " + StringProcessor.getInstance().iNumDepClass2);
                Log.v(TAG, "sDepClass3 = " + StringProcessor.getInstance().iNumDepClass3);

                int x = 0;
                int y = 0;
                int z = 0;

                for(int i=0;i<StringProcessor.getInstance().iDepInfoNum ;i++){
                    if (StringProcessor.getInstance().sDepClass[i].equals("1")){
                        StringProcessor.getInstance().sDepClass1[x] = StringProcessor.getInstance().sDepName[i];
                        StringProcessor.getInstance().sDepCodeClass1[x] = StringProcessor.getInstance().sDepCode[i];
                        StringProcessor.getInstance().sDepNameClass1[x] = StringProcessor.getInstance().sDepName[i];
                        x++;
                    }
                    else if (StringProcessor.getInstance().sDepClass[i].equals("2")){
                        StringProcessor.getInstance().sDepClass2[y] = StringProcessor.getInstance().sDepName[i];
                        StringProcessor.getInstance().sDepCodeClass2[y] = StringProcessor.getInstance().sDepCode[i];
                        StringProcessor.getInstance().sDepNameClass2[y] = StringProcessor.getInstance().sDepName[i];
                        y++;
                    }
                    else if (StringProcessor.getInstance().sDepClass[i].equals("3")){
                        StringProcessor.getInstance().sDepClass3[z] = StringProcessor.getInstance().sDepName[i];
                        StringProcessor.getInstance().sDepCodeClass3[z] = StringProcessor.getInstance().sDepCode[i];
                        StringProcessor.getInstance().sDepNameClass3[z] = StringProcessor.getInstance().sDepName[i];
                        z++;
                    }
                }

                break;
            case "GetKindInfo":
                String sKindInfo = obj.getString("KindInfo");
                dataArray = new JSONArray(sKindInfo);

                StringProcessor.getInstance().sKind = new String[dataArray.length()];
                StringProcessor.getInstance().sKindName = new String[dataArray.length()];

                for (int i = 0; i < dataArray.length(); i++) {
                    //接下來這兩行在做同一件事情，就是把剛剛JSON陣列裡的物件抓取出來
                    //並取得裡面的字串資料
                    //dataArray.getJSONObject(i)這段是在講抓取data陣列裡的第i個JSON物件
                    //抓取到JSON物件之後再利用.getString(“欄位名稱”)來取得該項value
                    //這裡的欄位名稱就是剛剛前面所提到的name:value的name
                    StringProcessor.getInstance().sKind[i] = dataArray.getJSONObject(i).getString("Kind");
                    StringProcessor.getInstance().sKindName[i] = dataArray.getJSONObject(i).getString("KindName");

                    Log.v(TAG, StringProcessor.getInstance().sKind[i]);
                    Log.v(TAG, StringProcessor.getInstance().sKindName[i]);
                }
                break;
            case "GetReserve":
                String sGetReserveResult = obj.getString("ReserveInfo");
                dataArray = new JSONArray(sGetReserveResult);

                StringProcessor.getInstance().iCheckReserveResultNum = dataArray.length();
                StringProcessor.getInstance().sCheckReserveResultSQ = new String[dataArray.length()];
                StringProcessor.getInstance().sCheckReserveResultDepName = new String[dataArray.length()];
                StringProcessor.getInstance().sCheckReserveResultKindName = new String[dataArray.length()];
                StringProcessor.getInstance().sCheckReserveResultReserveTime = new String[dataArray.length()];
                StringProcessor.getInstance().sCheckReserveResultPeriod = new String[dataArray.length()];

                for (int i = 0; i < dataArray.length(); i++) {
                    //接下來這兩行在做同一件事情，就是把剛剛JSON陣列裡的物件抓取出來
                    //並取得裡面的字串資料
                    //dataArray.getJSONObject(i)這段是在講抓取data陣列裡的第i個JSON物件
                    //抓取到JSON物件之後再利用.getString(“欄位名稱”)來取得該項value
                    //這裡的欄位名稱就是剛剛前面所提到的name:value的name
                    StringProcessor.getInstance().sCheckReserveResultSQ[i] = dataArray.getJSONObject(i).getString("SQ");
                    StringProcessor.getInstance().sCheckReserveResultDepName[i] = dataArray.getJSONObject(i).getString("DepName");
                    StringProcessor.getInstance().sCheckReserveResultKindName[i] = dataArray.getJSONObject(i).getString("KindName");
                    StringProcessor.getInstance().sCheckReserveResultReserveTime[i] = dataArray.getJSONObject(i).getString("ReserveTime");
                    StringProcessor.getInstance().sCheckReserveResultPeriod[i] = dataArray.getJSONObject(i).getString("Period");

                    Log.v(TAG, StringProcessor.getInstance().sCheckReserveResultSQ[i]);
                    Log.v(TAG, StringProcessor.getInstance().sCheckReserveResultDepName[i]);
                    Log.v(TAG, StringProcessor.getInstance().sCheckReserveResultKindName[i]);
                    Log.v(TAG, StringProcessor.getInstance().sCheckReserveResultReserveTime[i]);
                    Log.v(TAG, StringProcessor.getInstance().sCheckReserveResultPeriod[i]);
                }

                break;
            case "GetReserveDateSlot":
                String sReserveDateInfo = obj.getString("ReserveDateInfo");
                dataArray = new JSONArray(sReserveDateInfo);

                StringProcessor.getInstance().iDateNum = dataArray.length();
                //Reset first
                StringProcessor.getInstance().sDate = null;
                StringProcessor.getInstance().sReservePeriod = null;

                StringProcessor.getInstance().sDate = new String[dataArray.length()];
                StringProcessor.getInstance().sReservePeriod = new String[dataArray.length()];

                StringProcessor.getInstance().iPeriodNum = null;
                StringProcessor.getInstance().iPeriodNum = new int[dataArray.length()];

                //reservePeriodArray = null;
                //reservePeriodArray = new JSONArray[dataArray.length()];

                for (int i = 0; i < dataArray.length(); i++) {
                    //接下來這兩行在做同一件事情，就是把剛剛JSON陣列裡的物件抓取出來
                    //並取得裡面的字串資料
                    //dataArray.getJSONObject(i)這段是在講抓取data陣列裡的第i個JSON物件
                    //抓取到JSON物件之後再利用.getString(“欄位名稱”)來取得該項value
                    //這裡的欄位名稱就是剛剛前面所提到的name:value的name

                    //Get Date
                    StringProcessor.getInstance().sDate[i] = dataArray.getJSONObject(i).getString("Date");
                    //Log.v(TAG, "Date" + i + ":" + StringProcessor.getInstance().sDate[i]);

                    //reservePeriodArray[i] = new JSONArray(dataArray.getJSONObject(i).getString("ReservePeriod"));
                    String sReservePeriod = dataArray.getJSONObject(i).getString("ReservePeriod");

                    reservePeriodArray = new JSONArray(sReservePeriod);
                    /*StringProcessor.getInstance().sPeriod_SN = null;
                    StringProcessor.getInstance().sPeriod = null;
                    StringProcessor.getInstance().sRemainPerson = null;*/

                    
                    StringProcessor.getInstance().iPeriodNum[i] = reservePeriodArray.length();
                    if(i == 0) {
                        StringProcessor.getInstance().sPeriod_SN = new String[dataArray.length()][10];
                        StringProcessor.getInstance().sPeriod = new String[dataArray.length()][10];
                        StringProcessor.getInstance().sRemainPerson = new String[dataArray.length()][10];
                    }

                    //StringProcessor.getInstance().buileString(dataArray.length(), reservePeriodArray.length());

                    Log.v(TAG, "Period :" + StringProcessor.getInstance().iPeriodNum[i] + "\n");

                    for(int j = 0; j < StringProcessor.getInstance().iPeriodNum[i]; j++){
                        StringProcessor.getInstance().sPeriod_SN[i][j] = reservePeriodArray.getJSONObject(j).getString("Period_SN");
                        StringProcessor.getInstance().sPeriod[i][j] = reservePeriodArray.getJSONObject(j).getString("Period");
                        StringProcessor.getInstance().sRemainPerson[i][j] = reservePeriodArray.getJSONObject(j).getString("RemainPerson");
                        /*Log.v(TAG, "i=" + i + ", j=" + j + ", " + StringProcessor.getInstance().sPeriod_SN[i][j] + "\n");
                        Log.v(TAG, "i=" + i + ", j=" + j + ", " + StringProcessor.getInstance().sPeriod[i][j] + "\n");
                        Log.v(TAG, "i=" + i + ", j=" + j + ", " + StringProcessor.getInstance().sRemainPerson[i][j] + "\n");*/
                    }
                }

                /*for(int i =0; i<StringProcessor.getInstance().iDateNum;i++){
                    for(int j = 0; j < StringProcessor.getInstance().iPeriodNum[i]; j++) {
                        Log.v(TAG, "2i=" + i + ", j=" + j + ", " + StringProcessor.getInstance().sPeriod_SN[i][j] + "\n");
                        Log.v(TAG, "2i=" + i + ", j=" + j + ", " + StringProcessor.getInstance().sPeriod[i][j] + "\n");
                        Log.v(TAG, "2i=" + i + ", j=" + j + ", " + StringProcessor.getInstance().sRemainPerson[i][j] + "\n");
                    }
                }*/

                break;
            case "SendSMS":
                StringProcessor.getInstance().bSendSMSResult = obj.getBoolean("AddStatus");
                StringProcessor.getInstance().sSMSGUID = obj.getString("SMSGUID");
                Log.v(TAG, "Send SMS result = " + StringProcessor.getInstance().bSendSMSResult);
                break;

            default:
                Log.v(TAG, "default");
                break;
        }
    }
}

package com.postoffice.postofficev2;

import android.util.Log;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by peiranhsu on 2015/8/13.
 */
public class HTTPProcessor {
    String TAG = "HTTPProcessor";
    HttpURLConnection urlConnection = null;

    String sServiceName = null;

    public HTTPProcessor(String serviceName){
        this.sServiceName = serviceName;
    }

    public void setsServiceName(String serviceName){
        this.sServiceName = serviceName;
    }

    public String setupRequest() throws JSONException {
        String result = null;
        String url;

        switch (sServiceName){
            case "AddReserve":
                    url = Url_PostOffice.getInstance().URL_WEBSERVICE + "/AddReserve";
                try {
                    result =  doHTTPPost(url);
                    Log.v(TAG, "AddReserve:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "CancelReserve":
                    url = Url_PostOffice.getInstance().URL_WEBSERVICE + "/CancelReserve";
                try {
                    result =  doHTTPPost(url);
                    Log.v(TAG, "CancelReserve:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "CheckReserve":
                try {
                    url = Url_PostOffice.getInstance().URL_WEBSERVICE + "/CheckReserve";
                    result =  doHTTPPost(url);
                    Log.v(TAG, "CheckReserve:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "CheckSMSStatus":
                    url = Url_PostOffice.getInstance().URL_WEBSERVICE + "/CheckSMSStatus";
                try {
                    result =  doHTTPPost(url);
                    Log.v(TAG, "CheckSMSStatus:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "GetDepInfo":
                try {
                    result =  doHTTPGet(Url_PostOffice.getInstance().URL_WEBSERVICE + "/GetDepInfo");
                    Log.v(TAG, "GetDepInfo:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "GetKindInfo":
                try {
                    result = doHTTPGet(Url_PostOffice.getInstance().URL_WEBSERVICE + "/GetKindInfo");
                    Log.v(TAG, "GetKindInfo:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "GetReserve":
                try {
                    url = Url_PostOffice.getInstance().URL_WEBSERVICE + "/GetReserve";
                    result =  doHTTPPost(url);
                    Log.v(TAG, "GetReserve:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "GetReserveDateSlot":
                try {
                    result = doHTTPGet(Url_PostOffice.getInstance().URL_WEBSERVICE + "/GetReserveDateSlot");
                    Log.v(TAG, "GetReserveDateSlot:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "SendSMS":
                try {
                    url = Url_PostOffice.getInstance().URL_WEBSERVICE + "/SendSMS";
                    result =  doHTTPPost(url);
                    Log.v(TAG, "SendSMS:" + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            default:
                Log.v(TAG, "default");
                break;
        }

        if(!result.equals("httpFail")) {
            JSONProcessor.getInstance().parseJson(sServiceName, result);
        }

        return result;
    }

    private String doHTTPGet(String url) throws IOException {
        URL mUrl = new URL(url);

        urlConnection = (HttpURLConnection) mUrl.openConnection();
        urlConnection.setFollowRedirects(true);
        urlConnection.setConnectTimeout(100000);
        urlConnection.setReadTimeout(100000);
        urlConnection.setInstanceFollowRedirects(true);

        return parseHTTPResponse();
    }

    private String setupPostData(){
        String param = null;

        switch (sServiceName) {
            case "AddReserve":
                try {
                    param = "sReserveInfo=" + URLEncoder.encode(StringProcessor.getInstance().sReserveInfoAdd, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case "CancelReserve":
                try {
                    param = "sSQ=" + URLEncoder.encode(StringProcessor.getInstance().sSQ, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case "CheckReserve":
                try {
                    param = "sPhoneNo=" + URLEncoder.encode(StringProcessor.getInstance().sPhoneNo, "UTF-8");
                    param += "&sChooseDate=" + URLEncoder.encode(StringProcessor.getInstance().sDate[StringProcessor.getInstance().iDataSelected], "UTF-8");
                    param += "&sChoosePeriod_SN=" + URLEncoder.encode(StringProcessor.getInstance().sPeriod_SN[StringProcessor.getInstance().iDataSelected][StringProcessor.getInstance().iTimeSlotSelected], "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case "CheckSMSStatus":
                try {
                    param = "sSMSGUID=" + URLEncoder.encode(StringProcessor.getInstance().sSMSGUID, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case "GetReserve":
                try {
                    param = "sPhoneNo=" + URLEncoder.encode(StringProcessor.getInstance().sPhoneNo, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case "SendSMS":
                try {
                    param = "sPhoneNo=" + URLEncoder.encode(StringProcessor.getInstance().sPhoneNo, "UTF-8");
                    param += "&sSMSContent=" + URLEncoder.encode(StringProcessor.getInstance().sSMSContent, "UTF-8");
                    param += "&sSendDate=" + URLEncoder.encode(StringProcessor.getInstance().sSendDate, "UTF-8");
                    param += "&sSendTime=" + URLEncoder.encode(StringProcessor.getInstance().sSendTime, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
        }

        return param;
    }

    private String doHTTPPost(String url) throws IOException, JSONException {
        URL mUrl = new URL(url);

        Log.v(TAG, "param = " + setupPostData());
        Log.v(TAG, "url = " + url);

        urlConnection = (HttpURLConnection) mUrl.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setUseCaches(false);
        urlConnection.setConnectTimeout(10000);
        urlConnection.setReadTimeout(10000);
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        urlConnection.setRequestProperty("Content-Length", String.valueOf(setupPostData().getBytes().length));
        urlConnection.connect();

        DataOutputStream dos = new DataOutputStream(urlConnection.getOutputStream());
        dos.writeBytes(setupPostData());

        return parseHTTPResponse();
    }

    public String parseHTTPResponse() throws IOException {
        int HttpResult =urlConnection.getResponseCode();
        Log.v(TAG, "parseHTTPResponse = " + HttpResult);
        StringBuilder sb = new StringBuilder();
        String result = null;

        if(HttpResult == HttpURLConnection.HTTP_OK){
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    urlConnection.getInputStream(),"utf-8"));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();

            //System.out.println(""+sb.toString());

            result = sb.toString();

            int mJosonOffset = 0;

            for(int i=0;i<result.length();i++) {
                if (result.charAt(i) == '{') {
                    mJosonOffset = i;
                    break;
                }
            }

            result = result.substring(mJosonOffset);

        }else{
            //result = Integer.toString(HttpResult);
            result = "httpFail";
            Log.v(TAG, "parseHTTPResponse:" + urlConnection.getResponseMessage());
        }

        return result;
    }
}

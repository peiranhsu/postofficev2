package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class CancelReserveResultActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "CancelReserveResultActivity";

    TableLayout tableLayout;

    Button mButton_newReserve;
    Button mButton_requireReserve;
    Button mButton_cancwlReserve;
    Button mButton_backToRequire;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_result);

        tableLayout = (TableLayout) findViewById(R.id.tableLayout_resultList);

        mButton_newReserve = (Button) findViewById(R.id.button_newReserve);
        mButton_requireReserve= (Button) findViewById(R.id.button_requireReserve);
        mButton_cancwlReserve = (Button) findViewById(R.id.button_cancelReserve);
        mButton_backToRequire = (Button) findViewById(R.id.button_backToRequire);

        mButton_newReserve.setOnClickListener(this);
        mButton_requireReserve.setOnClickListener(this);
        mButton_cancwlReserve.setOnClickListener(this);

        mButton_backToRequire.setOnClickListener(this);

        mButton_newReserve.setOnTouchListener(this);
        mButton_requireReserve.setOnTouchListener(this);
        mButton_cancwlReserve.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        setupResultList();
    }

    private void setupResultList(){

        TableLayout result_list = (TableLayout)findViewById(R.id.tableLayout_resultList);
        result_list.setStretchAllColumns(true);
        TableLayout.LayoutParams row_layout = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams view_layout = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams button_layout = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);

        for(int i=0;i<StringProcessor.getInstance().iCheckReserveResultNum;i++){

            //Time
            TableRow tableRow_reserveTime = new TableRow(CancelReserveResultActivity.this);
            tableRow_reserveTime.setLayoutParams(row_layout);
            tableRow_reserveTime.setGravity(Gravity.CENTER_HORIZONTAL);


            TextView reserveTime = new TextView(CancelReserveResultActivity.this);
            reserveTime.setText("預約時段 : ");
            reserveTime.setLayoutParams(view_layout);

            TextView reserveTimeValue = new TextView(CancelReserveResultActivity.this);
            reserveTimeValue.setText(StringProcessor.getInstance().sCheckReserveResultReserveTime[i] + "/" + StringProcessor.getInstance().sCheckReserveResultPeriod[i]);
            reserveTimeValue.setLayoutParams(view_layout);

            tableRow_reserveTime.addView(reserveTime);
            tableRow_reserveTime.addView(reserveTimeValue);

            result_list.addView(tableRow_reserveTime);

            //PostOffice
            TableRow tableRow_reserveOffice = new TableRow(CancelReserveResultActivity.this);
            tableRow_reserveOffice.setLayoutParams(row_layout);
            tableRow_reserveOffice.setGravity(Gravity.CENTER_HORIZONTAL);

            TextView reserveOffice = new TextView(CancelReserveResultActivity.this);
            reserveOffice.setText("經辦郵局 : ");
            reserveOffice.setLayoutParams(view_layout);

            TextView reserveOfficeValue = new TextView(CancelReserveResultActivity.this);
            reserveOfficeValue.setText(StringProcessor.getInstance().sDepNameClass1[0] + "/" + StringProcessor.getInstance().sCheckReserveResultDepName[i] + "/" + StringProcessor.getInstance().sCheckReserveResultDepName[i]);
            reserveOfficeValue.setLayoutParams(view_layout);

            tableRow_reserveOffice.addView(reserveOffice);
            tableRow_reserveOffice.addView(reserveOfficeValue);

            result_list.addView(tableRow_reserveOffice);

            //Reserve type
            TableRow tableRow_reserveType = new TableRow(CancelReserveResultActivity.this);
            tableRow_reserveType.setLayoutParams(row_layout);
            tableRow_reserveType.setGravity(Gravity.CENTER_HORIZONTAL);

            TextView reserveType = new TextView(CancelReserveResultActivity.this);
            reserveType.setText("經辦郵局 : ");
            reserveType.setLayoutParams(view_layout);

            TextView reserveTypeValue = new TextView(CancelReserveResultActivity.this);
            reserveTypeValue.setText(StringProcessor.getInstance().sCheckReserveResultKindName[i]);
            reserveTypeValue.setLayoutParams(view_layout);

            tableRow_reserveType.addView(reserveType);
            tableRow_reserveType.addView(reserveTypeValue);

            result_list.addView(tableRow_reserveType);

            //Cancel button
            TableRow tableRow_cancelButton = new TableRow(CancelReserveResultActivity.this);
            tableRow_cancelButton.setLayoutParams(row_layout);
            tableRow_cancelButton.setGravity(Gravity.CENTER_HORIZONTAL);

            Button cancelButton = (Button) getLayoutInflater().inflate(R.layout.cancel_button, null);
            button_layout.height = 45;
            cancelButton.setLayoutParams(button_layout);

            tableRow_cancelButton.addView(cancelButton);

            result_list.addView(tableRow_cancelButton);

            //Space
            TableRow tableRow_space = new TableRow(CancelReserveResultActivity.this);
            tableRow_space.setLayoutParams(row_layout);
            tableRow_space.setGravity(Gravity.CENTER_HORIZONTAL);

            TextView space = new TextView(CancelReserveResultActivity.this);

            space.setLayoutParams(view_layout);

            tableRow_space.addView(space);

            result_list.addView(tableRow_space);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_get_number);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();

        switch (view.getId()){
            case R.id.button_newReserve:
                intent.putExtra("beforeActivity", "Main");
                intent.setClass(this, HttpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                break;
            case R.id.button_requireReserve:
                intent.setClass(CancelReserveResultActivity.this, CancelReserveResultActivity.class);
                startActivity(intent);
                break;
            case R.id.button_cancelReserve:
                intent.setClass(CancelReserveResultActivity.this, CancelReserveActivity.class);
                startActivity(intent);
                break;
            case R.id.button_backToRequire:
                intent.setClass(CancelReserveResultActivity.this, RequireReserveActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_newReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_new_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_new_reserve);
                }
                break;
            case R.id.button_requireReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_require_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_require_reserve);
                }
                break;
            case R.id.button_cancelReserve:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_cancel_reserve_push);
                }
                break;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent = new Intent();

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(this, MainActivity.class);

            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }
}

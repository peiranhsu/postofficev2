package com.postoffice.postofficev2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Date;


public class BusinessInfoActivity extends ActionBarActivity implements View.OnClickListener, View.OnTouchListener{

    private String TAG = "BusinessInfoActivity";


    Button mButton_postOfficeInfo;
    Button mButton_postOfficeIntrest;
    Button mButton_postOfficeBusinessService;
    Button mButton_postOfficeInfoRequire;
    Button mButton_postOfficeRequireQRCode;
    Button mButton_postOfficeQA;
    private int mServiceClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_info);

        this.mServiceClass = Url_PostOffice.getInstance().BUSSINESS_INFORMATION;

        mButton_postOfficeInfo = (Button) findViewById(R.id.button_post_office_info);
        mButton_postOfficeInfo.setOnClickListener(this);
        mButton_postOfficeInfo.setOnTouchListener(this);

        mButton_postOfficeIntrest = (Button) findViewById(R.id.button_post_office_interest);
        mButton_postOfficeIntrest.setOnClickListener(this);
        mButton_postOfficeIntrest.setOnTouchListener(this);

        mButton_postOfficeBusinessService = (Button) findViewById(R.id.button_business_service);
        mButton_postOfficeBusinessService.setOnClickListener(this);
        mButton_postOfficeBusinessService.setOnTouchListener(this);

        mButton_postOfficeInfoRequire = (Button) findViewById(R.id.button_require);
        mButton_postOfficeInfoRequire.setOnClickListener(this);
        mButton_postOfficeInfoRequire.setOnTouchListener(this);


        mButton_postOfficeRequireQRCode = (Button) findViewById(R.id.button_require_qr_code);
        mButton_postOfficeRequireQRCode.setOnClickListener(this);
        mButton_postOfficeRequireQRCode.setOnTouchListener(this);

        mButton_postOfficeQA = (Button) findViewById(R.id.button_post_office_QA);
        mButton_postOfficeQA.setOnClickListener(this);
        mButton_postOfficeQA.setOnTouchListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_network, menu);
        setupActionBar();

        return true;
    }

    public void setupActionBar(){
        Drawable drawable = getResources().getDrawable(R.drawable.action_bar_business_info);
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                break;
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()){
            case R.id.button_post_office_info:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][0][0]);
                break;
            case R.id.button_post_office_interest:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][1][0]);
                break;
            case R.id.button_business_service:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][2][0]);
                break;
            case R.id.button_businessInfo:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][3][0]);
                break;
            case R.id.button_require_qr_code:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][4][0]);
                break;
            case R.id.button_post_office_QA:
                intent = new Intent(Intent.ACTION_VIEW, Url_PostOffice.getInstance().mUri[this.mServiceClass][5][0]);
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.button_post_office_info:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_post_office_info_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_post_office_info);
                }
                break;
            case R.id.button_post_office_interest:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_post_office_interest_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_post_office_interest);
                }
                break;
            case R.id.button_business_service:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_business_service_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_business_service);
                }
                break;
            case R.id.button_require:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_require_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_require);
                }
                break;
            case R.id.button_require_qr_code:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_require_qr_code_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_require_qr_code);
                }
                break;
            case R.id.button_post_office_QA:
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    view.setBackgroundResource(R.drawable.icon_post_office_qa_push);
                }else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    view.setBackgroundResource(R.drawable.icon_post_office_qa);
                }
                break;
        }

        return false;
    }
}

package com.postoffice.postofficev2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class HttpActivity extends Activity implements View.OnClickListener{

    public static final int TIMER_STATE_SUCCESS = 0;
    public static final int TIMER_STATE_FAIL = 1;
    public static final int TIMER_STATE_RETRY = 2;

    public static final int CHECK_RESERVE_STATE_SUCCESS = 3;
    public static final int CHECK_RESERVE_STATE_OVERFLOW = 4;
    public static final int CHECK_RESERVE_STATE_DUPLICATED = 5;

    public static final int ADD_RESERVE_STATE_SUCCESS = 6;
    public static final int ADD_RESERVE_STATE_FAIL = 7;
    public static final int SEND_SMS_STATE_FAIL = 8;
    public static final int GET_RESERVE_NO_RECORD = 9;

    String TAG = "HttpActivity";

    String beforeActivity;
    Intent backIntent;
    Intent nextIntent;

    ProgressBar mProgressBar_underProgress;
    TextView mTextView_underProgress;
    Button mButton_backToMain;

    Timer timer = new Timer(true);
    Timer timerSMS = new Timer(true);
    myTimerTask timerTask;
    Handler mHandler;
    boolean retry = false;

    AsyncTask_HTTP mAsyncTask_getDepInfo;
    AsyncTask_HTTP mAsyncTask_getKindInfo;
    AsyncTask_HTTP mAsyncTask_getReserveDateSlot;

    AsyncTask_HTTP mAsyncTask_checkReserve;
    AsyncTask_HTTP mAsyncTask_addReserve;
    AsyncTask_HTTP mAsyncTask_sendSMS;
    AsyncTask_HTTP mAsyncTask_getReserve;
    AsyncTask_HTTP mAsyncTask_requireReserve;
    AsyncTask_HTTP mAsyncTask_cacelReserve;

    String result_getDepInfo;
    String result_getKindInfo;
    String result_getReserveDateSlot;

    String result_getReserve;
    String result_checkReserve;
    String result_addReserve;
    String result_requireReserve;
    String result_cancelReserve;

    String result_sendSMS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http);

        beforeActivity = getIntent().getStringExtra("beforeActivity");

        Log.v(TAG, "before activity = " + beforeActivity);

        mProgressBar_underProgress = (ProgressBar)findViewById(R.id.progressBar_underProgress);
        mTextView_underProgress = (TextView)findViewById(R.id.textView_underProgress);;
        mButton_backToMain = (Button)findViewById(R.id.button_backToMain);;
        mButton_backToMain.setOnClickListener(this);

        timerTask = new myTimerTask();
        timerTask.setActivity(beforeActivity);

        timer.schedule(timerTask, 1000, 99999);
    }

    @Override
    protected void onResume() {
        super.onResume();

        backIntent = new Intent();
        nextIntent = new Intent();
        //backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        nextIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        progressHTTP();

        mHandler = new Handler(){
            int i = 0;
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch(msg.what){
                    case TIMER_STATE_SUCCESS:
                        timer.cancel();
                        timerSMS.cancel();
                        timer = null;
                        timerTask = null;
                        startNextAvtivity();
                        break;
                    case TIMER_STATE_FAIL:
                        mProgressBar_underProgress.setVisibility(View.INVISIBLE);
                        mTextView_underProgress.setText("伺服器異常, 請稍候再試");
                        nextIntent = backIntent;
                        break;
                    case CHECK_RESERVE_STATE_SUCCESS:
                        mProgressBar_underProgress.setVisibility(View.VISIBLE);
                        mTextView_underProgress.setText("驗證碼傳送中...");

                        String SMSContent = StringProcessor.getInstance().buildSMSContent();
                        Log.v(TAG, "SMSContent = " + SMSContent);
                        Log.v(TAG, "randomSMSNumber = " + StringProcessor.getInstance().sRandomSMSNumber);
                        Log.v(TAG, "send date = " + StringProcessor.getInstance().sSendDate);
                        Log.v(TAG, "send time = " + StringProcessor.getInstance().sSendTime);

                        mAsyncTask_sendSMS = new AsyncTask_HTTP();
                        mAsyncTask_sendSMS.execute("SendSMS");

                        myTimerTask timerTaskSMS = new myTimerTask();
                        timerTaskSMS.setActivity("SendSMS");

                        timerSMS.schedule(timerTaskSMS, 1000, 99999);
                        break;
                    case CHECK_RESERVE_STATE_OVERFLOW:
                        mProgressBar_underProgress.setVisibility(View.INVISIBLE);
                        mTextView_underProgress.setText("超過預約次數");
                        break;
                    case CHECK_RESERVE_STATE_DUPLICATED:
                        mProgressBar_underProgress.setVisibility(View.INVISIBLE);
                        mTextView_underProgress.setText("已有預約記錄");
                        break;
                    case SEND_SMS_STATE_FAIL:
                        mProgressBar_underProgress.setVisibility(View.INVISIBLE);
                        mTextView_underProgress.setText("傳送簡驗證碼失敗");
                        break;
                    case ADD_RESERVE_STATE_FAIL:
                        mProgressBar_underProgress.setVisibility(View.INVISIBLE);
                        mTextView_underProgress.setText("新增預約失敗");
                        break;
                    case GET_RESERVE_NO_RECORD:
                        mTextView_underProgress.setText("查詢記錄失敗");
                        break;
                }
            }
        };
    }

    private void startNextAvtivity(){
        //timer.cancel();
        this.startActivity(nextIntent);
    }

    public void progressHTTP(){
        switch (beforeActivity){
            case "Main":
                backIntent.setClass(this, MainActivity.class);
                nextIntent.setClass(this, NewReserveActivity.class);

                mAsyncTask_getDepInfo = new AsyncTask_HTTP();
                mAsyncTask_getDepInfo.execute("GetDepInfo");

                mAsyncTask_getKindInfo = new AsyncTask_HTTP();
                mAsyncTask_getKindInfo.execute("GetKindInfo");

                mAsyncTask_getReserveDateSlot = new AsyncTask_HTTP();
                mAsyncTask_getReserveDateSlot.execute("GetReserveDateSlot");
                break;
            case "NewReserve" :
                backIntent.setClass(this, MainActivity.class);
                nextIntent.setClass(this, NewReserveCheckSMSActivity.class);

                //if(mAsyncTask_checkReserve == null) {
                    mAsyncTask_checkReserve = new AsyncTask_HTTP();
                //}

                mAsyncTask_checkReserve.execute("CheckReserve");
                break;
            case "CheckSMS" :
                backIntent.setClass(this, MainActivity.class);
                nextIntent.setClass(this, NewReserveSuccessActivity.class);

                //if(mAsyncTask_addReserve == null) {
                    mAsyncTask_addReserve = new AsyncTask_HTTP();
                //}

                mAsyncTask_addReserve.execute("AddReserve");
                break;
            case "RequireReserve" :
                backIntent.setClass(this, MainActivity.class);
                nextIntent.setClass(this, RequireReserveResultActivity.class);

                //if(mAsyncTask_getReserve == null) {
                    mAsyncTask_getReserve = new AsyncTask_HTTP();
                //}

                mAsyncTask_getReserve.execute("GetReserve");
                break;
            case "CancelReserve" :
                backIntent.setClass(this, MainActivity.class);
                nextIntent.setClass(this, CancelReserveResultActivity.class);

                //if(mAsyncTask_getReserve == null) {
                mAsyncTask_getReserve = new AsyncTask_HTTP();
                //}

                mAsyncTask_getReserve.execute("GetReserve");
                break;
        }
    }

    public class myTimerTask extends TimerTask
    {
        String mBeforeActivity;
        Message msg;

        public myTimerTask() {
            msg = new Message();
        }

        public void setActivity(String beforeActivity){
            mBeforeActivity = beforeActivity;
        }

        public void run()
        {
            msg.what = TIMER_STATE_FAIL;

            switch (mBeforeActivity){
                case "Main":
                    try {
                        result_getDepInfo = mAsyncTask_getDepInfo.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_getDepInfo = " + result_getDepInfo);

                        result_getKindInfo = mAsyncTask_getKindInfo.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_getKindInfo = " + result_getKindInfo);

                        result_getReserveDateSlot = mAsyncTask_getReserveDateSlot.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_getReserveDateSlot = " + result_getReserveDateSlot);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }

                    mAsyncTask_getDepInfo.cancel(true);
                    mAsyncTask_getKindInfo.cancel(true);
                    mAsyncTask_getReserveDateSlot.cancel(true);

                    if((result_getDepInfo != null) && (result_getKindInfo != null) && (result_getReserveDateSlot != null)){
                        if((!result_getDepInfo.equals("httpFail")) && (!result_getKindInfo.equals("httpFail")) &&
                                (!result_getReserveDateSlot.equals("httpFail"))){
                            //Time slot
                            StringProcessor.getInstance().sTimeSlotList = new String[StringProcessor.getInstance().iPeriodNum[StringProcessor.getInstance().iDataSelected]];

                            for(int j=0;j<StringProcessor.getInstance().iPeriodNum[StringProcessor.getInstance().iDataSelected];j++){
                                StringProcessor.getInstance().sTimeSlotList[j] = StringProcessor.getInstance().sPeriod[StringProcessor.getInstance().iDataSelected][j] + ", 剩餘人數"
                                        + StringProcessor.getInstance().sRemainPerson[StringProcessor.getInstance().iDataSelected][j];
                                Log.v(TAG, "j =" + j + ", " +StringProcessor.getInstance().sTimeSlotList[j]);
                            }

                            msg.what = TIMER_STATE_SUCCESS;
                        }
                    }
                    break;
                case "NewReserve":
                    try {
                        result_checkReserve = mAsyncTask_checkReserve.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_checkReserve = " + result_checkReserve);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }

                    mAsyncTask_checkReserve.cancel(true);

                    if((result_checkReserve != null) && !result_checkReserve.equals("httpFail")){
                        if(StringProcessor.getInstance().iCheckReserveResult != 3) {
                            msg.what = StringProcessor.getInstance().iCheckReserveResult + 3;//1:OVERFLOW -> 4, //2:DUPLICATED -> 5
                            break;
                        }
                        else{//Can add new reserve
                            msg.what = CHECK_RESERVE_STATE_SUCCESS;
                        }
                    }

                    break;
                case "SendSMS" :
                    try {
                        result_sendSMS = mAsyncTask_sendSMS.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_sendSMS = " + result_sendSMS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }

                    mAsyncTask_sendSMS.cancel(true);

                    if((result_sendSMS != null) && !result_sendSMS.equals("httpFail")){
                        if(StringProcessor.getInstance().bSendSMSResult != true) {
                            msg.what = ADD_RESERVE_STATE_FAIL;
                        }
                        else{
                            msg.what = TIMER_STATE_SUCCESS;
                        }
                    }

                    break;

                case "CheckSMS":
                    try {
                        result_addReserve = mAsyncTask_addReserve.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_addReserve = " + result_addReserve);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }

                    mAsyncTask_addReserve.cancel(true);

                    if((result_addReserve != null) && !result_addReserve.equals("httpFail")){
                        if(StringProcessor.getInstance().bAddReserveResult != true) {
                            msg.what = ADD_RESERVE_STATE_FAIL;
                        }
                        else{
                            msg.what = TIMER_STATE_SUCCESS;
                        }
                    }
                    break;
                case "RequireReserve" :
                    try {
                        result_getReserve = mAsyncTask_getReserve.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_getReserve = " + result_getReserve);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }

                    mAsyncTask_getReserve.cancel(true);

                    if((result_getReserve != null) && !result_getReserve.equals("httpFail")){
                        if(StringProcessor.getInstance().iCheckReserveResultNum == 0) {
                            msg.what = GET_RESERVE_NO_RECORD;
                        }
                        else {
                            msg.what = TIMER_STATE_SUCCESS;
                        }
                    }
                    break;
                case "CancelReserve" :
                    try {
                        result_getReserve = mAsyncTask_getReserve.get(3, TimeUnit.SECONDS);
                        Log.v(TAG, "result_getReserve = " + result_getReserve);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }

                    mAsyncTask_getReserve.cancel(true);

                    if((result_getReserve != null) && !result_getReserve.equals("httpFail")){
                        if(StringProcessor.getInstance().iCheckReserveResultNum == 0) {
                            msg.what = GET_RESERVE_NO_RECORD;
                        }
                        else {
                            msg.what = TIMER_STATE_SUCCESS;
                        }
                    }
                    break;
            }

            mHandler.sendMessage(msg);

            this.cancel();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        //timer.cancel();
    }

    @Override
    public void onClick(View view) {
        Log.v(TAG, "onClick");

        Intent intent = new Intent();
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        switch (view.getId()){
            case R.id.button_backToMain:
                timer.cancel();
                intent.setClass(this, MainActivity.class);
                break;
            default:
                break;
        }

        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Intent intent = new Intent();

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            timer.cancel();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(this, MainActivity.class);

            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }
}
